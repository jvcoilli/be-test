package org.rentasolutions.intake;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.rentasolutions.intake.controller.VehicleController;
import org.rentasolutions.intake.controller.WorkOrderController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class IntakeApplicationTest {

	@Autowired
	private VehicleController vehicleController;

	@Autowired
	private WorkOrderController workOrderController;

	@Test
	public void contextLoadsVehicleController() throws Exception {
		assertThat(vehicleController).isNotNull();
	}

	@Test
	public void contextLoadsWorkOrderController() throws Exception {
		assertThat(workOrderController).isNotNull();
	}

}
