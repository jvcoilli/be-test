insert into vehicle(id, vin, name, brand, created_at, updated_at) values(1, '4A3AK24FX6E028812', 'John Doessss', 'Mitsubishi', '2015-12-17', '2015-12-18');
insert into vehicle(id, vin, name, brand, created_at, updated_at) values(2, 'ZHWBU47S17LA02385', 'Jane Smith', 'Lamborghini', '2015-12-17', '2015-12-18');
insert into vehicle(id, vin, name, brand, created_at, updated_at) values(3, 'JTHCL5EF1B5010975', 'Walter White', 'Lexus', '2015-12-17', '2015-12-18');
insert into vehicle(id, vin, name, brand, created_at, updated_at) values(4, '1B3BK41B2BC227879', 'John Smith', 'Jaguar', '2015-12-17', '2015-12-18');
insert into vehicle(id, vin, name, brand, created_at, updated_at) values(5, 'SCBGH3ZA5FC042856', 'Ferry Bouman', 'Range Rover', '2015-12-17', '2015-12-18');

insert into workorder(id, vehicle_id, activity, description, quantity, price, created_at, updated_at) values(1, 1, 'changing oil filter', 'this is a description', 1, 250, '2015-12-17', '2015-12-18');