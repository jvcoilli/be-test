package org.rentasolutions.intake.controller;

import java.util.List;

import org.rentasolutions.intake.domain.Vehicle;
import org.rentasolutions.intake.exception.ResourceNotFoundException;
import org.rentasolutions.intake.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;


@RestController
public class VehicleController {

    
    @Autowired
    private VehicleRepository vehicleRepository;

    @Operation(summary = "Get all vehicles")
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", 
            description = "Found the vehicles", 
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = VehiclePage.class))}
        )
    })
    @GetMapping("/vehicles")
    public VehiclePage getAllVehicles(Pageable pageable) {
        Page<Vehicle> aPage = vehicleRepository.findAll(pageable);
        return new VehiclePage(aPage.getContent(), pageable, aPage.getContent().size());
    }

    @Operation(summary = "Get a vehicle by its vin")
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", 
            description = "Found the vehicle", 
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Vehicle.class))}
        ),
        @ApiResponse(responseCode = "404", 
            description = "Vehicle not found", 
            content = @Content
        )
    })
    @GetMapping("/vehicles/{vin}")
    public Vehicle getVehicleByVin(@PathVariable String vin) {
        return vehicleRepository.findByVin(vin).orElseThrow(() -> new ResourceNotFoundException("Vehicle with vin " + vin + " not found"));
    }
}

//Wrapper class
class VehiclePage extends PageImpl<Vehicle> {
    public VehiclePage(List<Vehicle> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }
}
