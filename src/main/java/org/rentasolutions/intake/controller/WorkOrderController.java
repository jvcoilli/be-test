package org.rentasolutions.intake.controller;

import org.rentasolutions.intake.domain.WorkOrder;
import org.rentasolutions.intake.exception.ResourceNotFoundException;
import org.rentasolutions.intake.repository.VehicleRepository;
import org.rentasolutions.intake.repository.WorkOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

import javax.validation.Valid;

@RestController
public class WorkOrderController {

    @Autowired
    private WorkOrderRepository workOrderRepository;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Operation(summary = "Get all workorders for vehicle")
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200",
            description = "Found the workOrders", 
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = WorkOrderPage.class))}
        )
    })
    @GetMapping("/vehicles/{vin}/workorders")
    public WorkOrderPage getAllWorkOrdersByVehicleVin(@PathVariable (value = "vin") String vin, Pageable pageable) {
        Page<WorkOrder> aPage = workOrderRepository.findByVehicleVin(vin, pageable);
        return new WorkOrderPage(aPage.getContent(), pageable, aPage.getContent().size());
    }

    @Operation(summary = "Add workOrder to a vehicle")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                description = "WorkOrder saved for vehicle",
                content = {@Content(mediaType = "application/json", schema = @Schema(implementation = WorkOrder.class))}
            ),
            @ApiResponse(responseCode = "404",
                description = "Vehicle Not Found",
                content = @Content
            )
    })
    @PostMapping("/vehicles/{vin}/workorders")
    public WorkOrder createWorkOrder(@PathVariable (value = "vin") String vin,
                                 @Valid @RequestBody WorkOrder workOrder) {
        return vehicleRepository.findByVin(vin).map(vehicle -> {
            workOrder.setVehicle(vehicle);
            return workOrderRepository.save(workOrder);
        }).orElseThrow(() -> new ResourceNotFoundException("Vehicle with vin " + vin + " not found"));
    }

}

//Wrapper class
class WorkOrderPage extends PageImpl<WorkOrder> {
    public WorkOrderPage(List<WorkOrder> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }
}
