package org.rentasolutions.intake;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@EnableJpaRepositories
@EnableJpaAuditing
@EntityScan("org.rentasolutions.intake.domain")
@OpenAPIDefinition(info=@Info(title="The Intake"))
public class IntakeApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntakeApplication.class, args);
	}

}
