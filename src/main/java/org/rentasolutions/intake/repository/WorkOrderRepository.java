package org.rentasolutions.intake.repository;

import org.rentasolutions.intake.domain.WorkOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WorkOrderRepository extends JpaRepository<WorkOrder, Long> {
    Page<WorkOrder> findByVehicleVin(String vehicleVin, Pageable pageable);
    Optional<WorkOrder> findByIdAndVehicleVin(Long id, String vehicleVin);
}
